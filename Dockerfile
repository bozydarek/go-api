FROM golang:alpine

COPY ./SimpleWebServce/* /usr/src/
WORKDIR /usr/src

RUN go build -o main . 
CMD ["./main"]

EXPOSE 8080
