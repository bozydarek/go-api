package main

import (
	"net/http"
	"net/url"
	"fmt"
	"log"
	"encoding/json"
)


func resp (writer http.ResponseWriter, request *http.Request) {
	fmt.Fprintln(writer, "<h1>SimpleWebService</h1>")
	fmt.Fprintln(writer, "YnkgQm96eWRhcg==")
	fmt.Fprintln(writer, "<h3>Example:</h3>")
	fmt.Fprintln(writer, "/routes?src=13.388860,52.517037&dst=13.428555,52.523219&dst=13.397634,52.529407&dst=17.777444,45.454333")
}

func handleRoutes(writer http.ResponseWriter, request *http.Request) {
	log.Print(request.RequestURI)

	uri, err := url.Parse(request.RequestURI)
	if err != nil {
		log.Println("URI parsing error: ", err)

		writer.WriteHeader(http.StatusBadRequest)
		errorResponse := serverErrorResponse{
			Code: "Bad Request",
			Message: "URI parsing error: " + err.Error(),
		}
		writer.Header().Set("Content-Type", "application/json")
		json.NewEncoder(writer).Encode(errorResponse)

		return
	}

	reqQuery := uri.Query()
	//fmt.Println(reqQuery)

	// check if data is valid
	validationError := validateQuery(reqQuery)
	if validationError != nil{
		log.Println("Query validation error:", validationError)

		writer.WriteHeader(http.StatusBadRequest)
		errorResponse := serverErrorResponse{
			Code: "Bad Request",
			Message: "Query validation error: " + validationError.Error(),
		}
		writer.Header().Set("Content-Type", "application/json")
		json.NewEncoder(writer).Encode(errorResponse)

		return
	}

	// make call to OSRM API
	goodResp, errResp, statusCode := checkDistances(reqQuery["src"][0], reqQuery["dst"])

	// send response
	if statusCode != 200 {
		writer.WriteHeader(statusCode) // Respond with same status code as OSMR service
		writer.Header().Set("Content-Type", "application/json")
		json.NewEncoder(writer).Encode(errResp)
	}else{
		writer.Header().Set("Content-Type", "application/json")
		json.NewEncoder(writer).Encode(goodResp)
	}
}

func main()  {
	fmt.Println("Starting SimpleWebService")

	http.HandleFunc("/", resp)
	http.HandleFunc("/routes", handleRoutes)

	err := http.ListenAndServe(":8080", nil)

	if err != nil {
		log.Fatalln("Server error on start: ", err)
	}
}