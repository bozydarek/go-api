package main

import (
	"log"
	"fmt"
	"net/url"
	"net/http"
	"encoding/json"
	"time"
	"sort"
	"errors"
)

const OSMR_url = "http://router.project-osrm.org/route/v1/driving/"
const time_between_calls = 200 // Millisecond
const amounts_of_tries = 5


type OSRMResponse struct {
	Code 		string             `json:"code"`
	Routes 		[]RoutesInResponse `json:"routes"`
	Message 	string             `json:"message"`

}

type RoutesInResponse struct {
	Destination string 	`json:"destination"`
	Duration 	float64 `json:"duration"`
	Distance 	float64 `json:"distance"`
}

type serverResponse struct{
	Source 		string             `json:"source"`
	Routes 		[]RoutesInResponse `json:"routes"`
}

type serverErrorResponse struct{
	Code 		string 	`json:"code"`
	Message 	string 	`json:"message"`

}

func validateQuery(values url.Values) error {

	if val, ok := values["src"]; ok {
		if len(val) > 1 {
			return errors.New("too many sources")
		}
	}else{
		return errors.New("no source points provided in request")
	}

	if _, ok := values["dst"]; ! ok {
		return errors.New("no destination points provided in request")
	}

	return nil
}


func checkDistances(src string, dst []string ) (*serverResponse, *serverErrorResponse, int) {
	//log.Println(src, dst)

	var routes []RoutesInResponse

	for _, dest := range dst {

		var response *OSRMResponse
		var httpStatusCode int

		for i := 0; i < amounts_of_tries; i++ {
			response, httpStatusCode = makeQueryToOSMR(src, dest)
			if httpStatusCode == 200 {
				break
			}
			time.Sleep(time.Duration(time_between_calls)*time.Millisecond)
		}

		if httpStatusCode != 200 {
			errorResponse := serverErrorResponse{
				Code: response.Code,
				Message: response.Message,
			}

			return nil, &errorResponse, httpStatusCode
		}

		//log.Println(response)
		route := response.Routes[0] // Take first one only
		route.Destination = dest

		routes = append(routes, route)

	}

	// sorted by driving time and distance (if time is equal).
	sort.Slice(routes, func(i, j int) bool {
		if routes[i].Duration == routes[j].Duration {
			return routes[i].Distance < routes[j].Distance
		}else{
			return routes[i].Duration < routes[j].Duration
		}
	})

	response := serverResponse{
		Source: src,
		Routes: routes,
	}

	return &response, nil, http.StatusOK
}

func makeQueryToOSMR(src string, dest string ) (*OSRMResponse, int) {
	apiReq := fmt.Sprintf(OSMR_url + src + ";" + dest + "?overview=false")
	//log.Println(apiReq)

	req, err := http.NewRequest("GET", apiReq, nil)
	if err != nil {
		log.Println("NewRequest: ", err)
		return nil, http.StatusInternalServerError
	}

	client := &http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		log.Println("Do: ", err)
		return nil, http.StatusInternalServerError
	}

	httpStatusCode := resp.StatusCode

	defer resp.Body.Close()

	var record *OSRMResponse
	if err := json.NewDecoder(resp.Body).Decode(&record); err != nil {
		log.Println(err)
	}

	return record, httpStatusCode
}