package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"io/ioutil"
	"os"
)

// simple Go application, that makes call to API (http://apilayer.net/api/validate) and parse received message.
// This API check if phone number is valid and provide some information about it

type ErrorRec struct {
	Code	int    `json:"code"`
	Type	string `json:"type"`
	Info    string `json:"info"`
}

type Numverify struct {
	Success				bool   `json:"success"`
	Error 				ErrorRec

	Valid               bool   `json:"valid"`
	Number              string `json:"number"`
	LocalFormat         string `json:"local_format"`
	InternationalFormat string `json:"international_format"`
	CountryPrefix       string `json:"country_prefix"`
	CountryCode         string `json:"country_code"`
	CountryName         string `json:"country_name"`
	Location            string `json:"location"`
	Carrier             string `json:"carrier"`
	LineType            string `json:"line_type"`
}

func checkPhoneNumber(phone string, secretKey string) {

	// QueryEscape escapes the phone string so
	// it can be safely placed inside a URL query
	safePhone := url.QueryEscape(phone)
	log.Print(safePhone)

	apiUrl := fmt.Sprintf("http://apilayer.net/api/validate?access_key=%s&number=%s", secretKey, safePhone)

	// Build the request
	req, err := http.NewRequest("GET", apiUrl, nil)
	if err != nil {
		log.Fatal("NewRequest: ", err)
		return
	}

	// For control over HTTP client headers,
	// redirect policy, and other settings,
	// create a Client
	// A Client is an HTTP client
	client := &http.Client{}

	// Send the request via a client
	// Do sends an HTTP request and
	// returns an HTTP response
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal("Do: ", err)
		return
	}

	// Callers should close resp.Body
	// when done reading from it
	// Defer the closing of the body
	defer resp.Body.Close()

	// Fill the record with the data from the JSON
	var record Numverify

	// Use json.Decode for reading streams of JSON data
	if err := json.NewDecoder(resp.Body).Decode(&record); err != nil {
		log.Println(err)
	}

	//log.Println(record.Success)
	//
	if record.Valid == false && record.Success == false {
		log.Println("Unsuccessful call to API. Error recived:")
		fmt.Println("Error code = ", record.Error.Code)
		fmt.Println("Error type = ", record.Error.Type)
		fmt.Println("Error info = ", record.Error.Info)

		os.Exit(1)
	}

	fmt.Println("Phone No. = ", record.InternationalFormat)
	fmt.Println("Country   = ", record.CountryName)
	fmt.Println("Location  = ", record.Location)
	fmt.Println("Carrier   = ", record.Carrier)
	fmt.Println("LineType  = ", record.LineType)
}


func main() {
	fmt.Println("Hello there!")

	secretKey := "SOME_SECRET"

	fileBytes, err := ioutil.ReadFile("API_KEY") // just pass the file name
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	//fmt.Println("#" + string(fileBytes) + "#")
	secretKey = string(fileBytes)

	phones := []string{"48505571510", "14158586273", "48715005050"}

	for _, phone := range phones{
		checkPhoneNumber(phone, secretKey)
	}
}
