# GO warm-up

This repository contains:

* **PoC** - first test application that validates phone numbers using `apilayer.net/api/validate`
* **SimpleWebServce** - second application, solution for coding assignment. Service's task is to determine the distance from given source to destinations.

> This programs **don't** use any 3rd party packages or frameworks.

## How to run SimpleWebServce

### Option 1 - from source code
``` bash
    $ git clone https://bozydarek@bitbucket.org/bozydarek/go-api.git
    $ cd go-api/SimpleWebServce
    $ go run *.go
```

Server will start listening on port **8080**. If any error occurs during starting the server, program will print the error message and exit. 

#### Using example

With curl:

``` bash
curl -i 'localhost:8080/routes?src=13.388860,52.517037&dst=13.428555,52.523219&dst=13.397634,52.529407&dst=17.777444,45.454333'
```

Or simple with web browser:
http://localhost:8080/routes?src=13.388860,52.517037&dst=13.428555,52.523219&dst=13.397634,52.529407


### Option 2 - using Docker image

``` bash
    $ docker pull bozydarek/simple-web-service
    $ docker run  -d --name my-sws \
            -p 7777:8080 \
            bozydarek/simple-web-service
```

#### Using example

Same as in Option 1, remember about the port number (in example above port numer is 7777).